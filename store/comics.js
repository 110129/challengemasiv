export const state=()=>({
    comic:[],
    lastestComic:[]
  })

export const getters={
  getComic(state){return state.comic},
  getlastestComic(state){return state.lastestComic}
}
export const mutations={
  setComic: function (state, data) {
    state.comic = data
  },
  setLastestComic: function(state, data) {
    state.lastestComic.push(data)
  }
}
  
export const actions={
  async recuperarComic({commit} ){
    const nro = Math.floor(Math.random() * (2543) ) + 1;
    try{
        console.log('recuperar_comincs');
        await this.$axios.$get(`api/${nro}/info.0.json`).then(response => {
            console.log('responces',response);
            commit('setComic',response)
        })
    } catch(error){
      console.log(error)
    }
  },
  async addLatestComic({commit},data){
    commit('setLastestComic',data)
    console.log('ultimo',data);
  }
}